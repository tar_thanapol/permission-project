package th.co.digio.permission.project.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.co.digio.movies.project.entity.Movie;
import th.co.digio.movies.project.repository.MovieRepository;

@Component
public class MovieResolver implements GraphQLResolver<Movie> {
    @Autowired
    private MovieRepository movieRepository;
}

package th.co.digio.permission.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import th.co.digio.permission.project.entity.UserPermissionRoleRef;

@Repository
public interface UserPermissionRoleRefRepository extends JpaRepository<UserPermissionRoleRef, Long> {
}

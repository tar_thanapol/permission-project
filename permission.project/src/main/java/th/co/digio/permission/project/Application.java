package th.co.digio.permission.project;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = {"th.co.digio.permission.project.repository", "th.co.digio.movies.project.repository"})
@ComponentScan(basePackages = {"th.co.digio.permission.project"})
@EntityScan(basePackages = {"th.co.digio.permission.project.entity","th.co.digio.movies.project.entity"})
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}

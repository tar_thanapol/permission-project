package th.co.digio.permission.project.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.co.digio.permission.project.entity.User;
import th.co.digio.permission.project.repository.UserRepository;

@Component
public class UserMutation implements GraphQLMutationResolver {

    private UserRepository userRepository;
    @Autowired
    public UserMutation(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User createUser(String username, String password, String email) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        return userRepository.save(user);
    }


}

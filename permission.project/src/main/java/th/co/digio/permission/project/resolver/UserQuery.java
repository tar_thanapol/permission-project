package th.co.digio.permission.project.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.co.digio.movies.project.entity.Movie;
import th.co.digio.movies.project.repository.MovieRepository;
import th.co.digio.permission.project.entity.User;
import th.co.digio.permission.project.repository.UserRepository;

@Component
public class UserQuery implements GraphQLQueryResolver {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MovieRepository movieRepository;

    public Iterable<Movie> findAllMovie(){
        return movieRepository.findAll();
    }
    public Iterable<User>  findAllUser(){
        return userRepository.findAll();
    }
}

package th.co.digio.permission.project.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import th.co.digio.permission.project.entity.User;
import th.co.digio.permission.project.repository.UserRepository;

@Component
public class UserResolver implements GraphQLResolver<User> {
    @Autowired
    private UserRepository userRepository;
}
